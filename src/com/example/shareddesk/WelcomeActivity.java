package com.example.shareddesk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.example.shareddesk.common.Constants;

public class WelcomeActivity extends Activity {
  
  private static final String TAG = WelcomeActivity.class.getCanonicalName();
  
  
  @Override
  public void onCreate( Bundle bundle ) {
    super.onCreate( bundle );
    setContentView( R.layout.activity_welcome );
    
    Button cont = (Button) findViewById( R.id.btn_check_peer );
    final EditText ipAddress = (EditText) findViewById(  R.id.ip_address );
    final EditText port = (EditText) findViewById(  R.id.port );;
    cont.setOnClickListener( new OnClickListener() {
      
      @Override
      public void onClick( View v ) {
        Intent intent = new Intent( WelcomeActivity.this, FileListActivity.class );
        Bundle extras = new Bundle();
        extras.putString( Constants.EXTRA_IP, ipAddress.getText().toString());
        extras.putInt( Constants.EXTRA_PORT, Integer.parseInt( port.getText().toString()) );
        intent.putExtras( extras );
        startActivity( intent );
      }
      
    } );
  }
  
  
  @Override
  public boolean onCreateOptionsMenu( Menu menu ) {
    // don't call super, no menu for this activity
    return true;
  }
  
}

package com.example.shareddesk;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FileListAdapter extends BaseAdapter {
  
  private List<File> mFiles;
  private Context mContext;
  private LayoutInflater mInflater;
  
  public FileListAdapter( Context context ) {
    super();
    mContext = context;
    mInflater = LayoutInflater.from( context );
    mFiles = new ArrayList<File>();

  }

  public void setFiles( Map<Long, File> files ) {
    mFiles.clear();
    mFiles.addAll( files.values() );
  }
  
  @Override
  public int getCount() {
    return mFiles == null ? 0 : mFiles.size();
  }
  
  @Override
  public Object getItem( int position ) {
    if ( mFiles == null || position >= getCount() ) {
      return null;
    }
    return mFiles.get( position );
  }
  
  @Override
  public long getItemId( int position ) {
    File file = (File) getItem( position );
    return file == null ? 0 : file.getId();
  }
  
  @Override
  public View getView( int position, View view, ViewGroup parent ) {
    Holder holder;
    if ( view == null ) {
      view = mInflater.inflate( R.layout.item_filelist, null );
      holder = new Holder( view );
      view.setTag( holder );
    } else {
      holder = (Holder) view.getTag();
    }
    
    File file = (File) getItem( position );

    //File.Type type = file.getType();
    holder.mTitle.setText( file.getFilename() );
    holder.mIcon.setImageDrawable( mContext.getResources().getDrawable( file.getType().getIconId() ) );
    holder.mIcon.setClickable( false );
    view.setBackgroundResource( 0 );
    
    return view;
  }
  
  
  
  public void setGoals( Map<Long, File> goals ) {
    mFiles.clear();
    mFiles.addAll( goals.values() );
    notifyDataSetChanged();
  }
  
  
  private class Holder {
    
    ImageView mIcon;
    TextView mTitle;
    
    private Holder( View view ) {
      mTitle = (TextView) view.findViewById( R.id.item_title );
      mIcon = (ImageView) view.findViewById( R.id.item_icon );

    }
    
  }
}


package com.example.shareddesk.peer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import com.example.shareddesk.common.BytesConvertUtils;
import com.example.shareddesk.common.UDPConstants;

public class Sender {
  
  private DatagramSocket mSocket;
  
  public Sender(DatagramSocket socket){
    mSocket = socket;
  }
  
  public void sendRequestFileInfo(final String IpAddress, final int port){
    //Sending UDP packet with a thread, since android does not allow you to do network things on the Main thread
    Thread thread = new Thread(new Runnable() {
      @Override
      public void run() {
          try {
              InetAddress IPAddress =  InetAddress.getByName(IpAddress);

              //while (true)
              // {
              byte[] commandByte = {UDPConstants.CommandByte.S_STUN_REQUEST};
              byte[] guidByteArray = BytesConvertUtils.convertintToBytes(100);
              byte[] send_data = BytesConvertUtils.combineBytes(commandByte, guidByteArray);
              //System.out.println("Type Something (q or Q to quit): ");

              DatagramPacket send_packet = new DatagramPacket(send_data, send_data.length , IPAddress, port);

              mSocket.send(send_packet);
          } catch (IOException e) {
              e.printStackTrace();
          }
      }

    });
    thread.start();
  }
}

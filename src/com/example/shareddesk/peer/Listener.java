package com.example.shareddesk.peer;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;

import com.example.shareddesk.File;
import com.example.shareddesk.File.Type;
import com.example.shareddesk.FileListActivity.FileInfoTask;

public class Listener extends Thread {
  
  private Activity mActivity;
  private boolean isRunning = true;
  private String lastMessage = "";
  private Handler mMainHandler;
  private DatagramSocket mSocket = null;


  public Listener(Handler handler, DatagramSocket socket){
     mMainHandler = handler;
     mSocket = socket;
  }
  
  public void run() {
    /*
    String message;
    byte[] lmessage = new byte[Peer.MAX_UDP_DATAGRAM_LEN];
    DatagramPacket packet = new DatagramPacket(lmessage, lmessage.length);

    try {

        while(isRunning) {
            mSocket.receive(packet);
            message = new String(lmessage, 0, packet.getLength());
            lastMessage = message;
            handleFileInfo();
        }
    } catch (Throwable e) {
        e.printStackTrace();
    }
    
    if (mSocket != null) {
      mSocket.close();
    }
     */
    handleFileInfo();

  }

  public void kill() { 
      isRunning = false;
  }

  public String getLastMessage() {
      return lastMessage;
  }
  
  @SuppressLint( "UseSparseArrays" )
  public void handleFileInfo() {
    HashMap<Long, File> files = getDummyData(); //new HashMap<Long, File>();
    
    
    FileInfoTask fileInfoTask = new FileInfoTask(files);
    mMainHandler.post( fileInfoTask );
  }
  
  private HashMap<Long, File> getDummyData(){
    HashMap<Long, File> filelist = new HashMap<Long, File>();
    filelist.put( 0l, new File( 0, Type.get( Type.JPEG ), "dummy1.jpeg", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 1l, new File( 1, Type.get( Type.PDF ), "dummy2.pdf", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 2l, new File( 2, Type.get( Type.XML ), "dummy3.xml", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 3l, new File( 3, Type.get( Type.JPEG ), "dummy4.jpeg", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 4l, new File( 4, Type.get( Type.PDF ), "dummy5.pdf", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 5l, new File( 5, Type.get( Type.XML ), "dummy6.xml", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 6l, new File( 6, Type.get( Type.JPEG ), "dummy7.jpeg", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 7l, new File( 7, Type.get( Type.PDF ), "dummy8.pdf", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 8l, new File( 8, Type.get( Type.XML ), "dummy9.xml", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 9l, new File( 9, Type.get( Type.JPEG ), "dummy10.jpeg", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 10l, new File( 10, Type.get( Type.PDF ), "dummy11.pdf", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 11l, new File( 11, Type.get( Type.XML ), "dummy12.xml", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 12l, new File( 12, Type.get( Type.JPEG ), "dummy13.jpeg", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 13l, new File( 13, Type.get( Type.PDF ), "dummy14.pdf", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 14l, new File( 14, Type.get( Type.XML ), "dummy15.xml", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 15l, new File( 15, Type.get( Type.JPEG ), "dummy16.jpeg", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 16l, new File( 16, Type.get( Type.PDF ), "dummy17.pdf", System.currentTimeMillis(), 2.0 ) );
    filelist.put( 17l, new File( 17, Type.get( Type.XML ), "dummy18.xml", System.currentTimeMillis(), 2.0 ) );
    
    return filelist;
  }


  
}

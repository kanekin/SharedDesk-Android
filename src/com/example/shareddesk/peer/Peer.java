package com.example.shareddesk.peer;

import java.net.DatagramSocket;
import java.net.SocketException;

import com.example.shareddesk.common.UDPConstants;

import android.os.Handler;

public class Peer {
  public static final int UDP_PORT = 2004;
  public static final int MAX_UDP_DATAGRAM_LEN = 1500;
  
  private DatagramSocket mSocket;
  private Listener mListener;
  private Sender mSender;
  
  public Peer(Handler handler)
  {
    try {
      mSocket = new DatagramSocket(UDP_PORT);
      mListener = new Listener(handler, mSocket);
      mSender = new Sender(mSocket);
      
      mListener.start();
      
    } catch ( SocketException e ) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  public void sendRequestFileInfo(String ipAddress, int port){
    mSender.sendRequestFileInfo( ipAddress, port);
  }
  
  public void closeSocket(){
    mListener.kill();
  }
}

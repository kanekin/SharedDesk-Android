package com.example.shareddesk;


public class File {
  
  private long mId;
  private Type mType;
  private String mFilename;
  private long mStamp;
  private double mSize;
  

  public File(long id, Type type, String filename, long stamp, double size){
    mId = id;
    mType = type;
    mFilename = filename;
    mStamp = stamp;
    mSize = size;
  }

  public long getId(){
    return mId;
  }
  
  public Type getType(){
    return mType;
  }
  
  public String getFilename(){
    return mFilename;
  }
  
  public long getStamp(){
    return mStamp;
  }
  
  public double getSize(){
    return mSize;
  }
  
  
  public static class Type {
    
    public static final String JPEG = "JPEG";
    public static final String PNG = "PNG";
    public static final String XML = "XML";
    public static final String WORD = "WORD";
    public static final String PDF = "PDF";
    public static final String UNKNOWN = "UNKNOWN";

    private String mKey;
    private int mIconId;
    
    public static final Type get( String type ) {
      Type result = null;
      for ( int i = 0; i < ALL.length; i++ ) {
        if ( ALL[ i ].mKey.equals( type ) ) {
            result = ALL[ i ];
        }
      }
      return result;
    }
    
    public static final Type[] ALL = new Type[] {
      // movement
      new Type( JPEG, R.drawable.icon_file_image ),
      new Type( PDF, R.drawable.icon_file_document),
      new Type( XML, R.drawable.icon_file_code)
      //new Type( PNG, ), 
    };
    
    private Type( String key, int iconId) {
      mKey = key;
      mIconId = iconId;
    }
    
    public int getIconId(){
      return mIconId;
    }
    
    public String getKey(){
      return getKey();
    }
  }
  
}

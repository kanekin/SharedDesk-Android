package com.example.shareddesk;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.example.shareddesk.common.Constants;
import com.example.shareddesk.common.UDPConstants;
import com.example.shareddesk.peer.Peer;

public class FileListActivity extends Activity {
  
  protected ListView mListView = null;
  private static FileListAdapter mAdapter = null;
  private int mAdapterSize = -1;
  private Peer mPeer;
  private Handler mHandler;
  private static Map<Long, File> mFilelist;

  
  @Override
  protected void onCreate( Bundle savedInstanceState ) {
    super.onCreate( savedInstanceState );
    setContentView( R.layout.activity_file_list );
    
    
    //Get parameter from the first page
    Bundle extras = getIntent().getExtras();
    String ipAddress = extras.getString( Constants.EXTRA_IP, "0.0.0.0" );
    int port = extras.getInt( Constants.EXTRA_PORT, 0 );
    
    Toast.makeText( this, ipAddress + port , Toast.LENGTH_SHORT ).show();
    
    mListView = (ListView) findViewById( R.id.file_list_view );
    mAdapter = new FileListAdapter( this );
    mListView.setAdapter( mAdapter );
    mListView.setOnItemClickListener( new OnItemClickListener() {

      @Override
      public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {
        File file = (File) parent.getItemAtPosition(position);
        
        //DO what ever you want
        Toast.makeText( FileListActivity.this, file.getFilename(), Toast.LENGTH_SHORT ).show();
        mPeer.sendRequestFileInfo( UDPConstants.STUN_SERVER_IP, UDPConstants.STUN_SERVER_PORT);
      }
    } );
  }

  
  @Override
  protected void onResume() {
    super.onResume();
    mHandler = new Handler();
    mPeer = new Peer(mHandler);
  }
  
  @Override
  public boolean onCreateOptionsMenu( Menu menu ) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate( R.menu.file_list, menu );
    return true;
  }
  
  @Override
  public boolean onOptionsItemSelected( MenuItem item ) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();
    if ( id == R.id.action_settings ) {
      return true;
    }
    return super.onOptionsItemSelected( item );
  }
  
  @Override
  protected void onStop() {
    //TODO: think when we should stop listening
    //stopListening
    mPeer.closeSocket();
    super.onStop();
  }

  public static class FileInfoTask implements Runnable{
    
    public FileInfoTask(HashMap<Long, File> files) {
      mFilelist = files;
    }
    
    @Override
    public void run() {
      mAdapter.setFiles( mFilelist );
    }
  }


}

package com.example.shareddesk.common;

public class Constants {
  
  public static final String EXTRA_IP = "extra_ipAddress";
  public static final String EXTRA_PORT = "extra_port";
  
}

package com.example.shareddesk.common;

/**
 * Created by tatsuyakaneko on 04/01/15.
 */
public class UDPConstants
{

    public static final String STUN_SERVER_IP = "83.86.76.83";// John "92.109.173.161";//"192.168.0.107"; Rijswijk "83.86.76.83"
    public static final int STUN_SERVER_PORT = 4000;
    // define all the possible commands


    public final class CommandByte{
        public static final byte ERROR = (byte)0;
        public static final byte K_PING_REQUEST = (byte)1;
        public static final byte K_PING_RESPONSE = (byte)2;
        public static final byte K_ROUTINGTABLE_REQUEST = (byte)3;
        public static final byte K_ROUTINGTABLE_RECEIVED = (byte)4;
        public static final byte K_CLOSESTPEER_REQUEST = (byte)5;
        public static final byte K_CLOSESTPEER_RESPONSE = (byte)6;
        public static final byte K_JOIN = (byte)7;
        public static final byte K_LEAVE = (byte)8;
        public static final byte F_FILEINFO = (byte)9;
        public static final byte F_FILETRANSFER_REQUEST = (byte)10;
        public static final byte F_FILETRANSFER_RESPONSE = (byte)11;
        public static final byte F_FILEPART_REQUEST = (byte)12;
        public static final byte F_FILEPART_RESPONSE = (byte)13;
        public static final byte F_SENDFILE_REQUEST = (byte)14;
        public static final byte S_STUN_REQUEST = (byte)15;
        public static final byte S_STUN_RESPONSE = (byte)16;
        public static final byte S_STUN_PUNCH_REQUEST = (byte)17;
        public static final byte S_STUN_PUNCH_REQUEST_FORWARD = (byte)18;
        public static final byte S_STUN_PUNCH = (byte)19;
        public static final byte S_STUN_KEEP_ALIVE = (byte)20;
    }
    /*
    public static enum commandByte{
                ERROR,
                K_PING_REQUEST,
                K_PING_RESPONSE,
                K_ROUTINGTABLE_REQUEST,
                K_ROUTINGTABLE_RECEIVED,
                K_CLOSESTPEER_REQUEST,
                K_CLOSESTPEER_RESPONSE,
                K_JOIN,
                K_LEAVE,
                F_FILEINFO,
                F_FILETRANSFER_REQUEST,
                F_FILETRANSFER_RESPONSE,
                F_FILEPART_REQUEST,
                F_FILEPART_RESPONSE,
                F_SENDFILE_REQUEST,
                S_STUN_REQUEST,
                S_STUN_RESPONSE,
                S_STUN_PUNCH_REQUEST,
                S_STUN_PUNCH_REQUEST_FORWARD,
                S_STUN_PUNCH,
                S_STUN_KEEP_ALIVE
    };
    */
}

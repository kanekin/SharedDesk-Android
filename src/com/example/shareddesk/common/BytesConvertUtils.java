package com.example.shareddesk.common;

import java.nio.ByteBuffer;

/**
 * Created by tatsuyakaneko on 04/01/15.
 */
public class BytesConvertUtils {
    /// <summary>
    /// Byte[] Functionality
    /// </summary>
    public static byte[] combineBytes(byte[] first, byte[] second)
    {
        byte[] ret = new byte[first.length + second.length];
        System.arraycopy(first, 0, ret, 0, first.length);
        System.arraycopy(second, 0, ret, first.length, second.length);
        return ret;
    }

    public static byte[] combineBytes(byte[] first, byte[] second, byte[] third)
    {
        return combineBytes(combineBytes(first, second),third);
    }

    public static byte[] combineBytes(byte[] first, byte[] second, byte[] third, byte[] fourth)
    {
        return combineBytes(combineBytes(first, second, third), fourth);
    }

    public static byte[] combineBytes(byte[] first, byte[] second, byte[] third, byte[] fourth, byte[] fifth)
    {
        return combineBytes(combineBytes(first, second, third, fourth), fifth);
    }

    public static byte[] combineBytes(byte[] first, byte[] second, byte[] third, byte[] fourth, byte[] fifth, byte[] sixth)
    {
        return combineBytes(combineBytes(first, second, third, fourth, fifth), sixth);
    }

    public static byte[] convertStringToBytes(String str)
    {
        byte[] bytes = new byte[str.length() * Character.SIZE];
        System.arraycopy(str.toCharArray(), 0, bytes, 0, bytes.length);
        return bytes;
    }

    public static String convertByteToString(byte[] bytes)
    {
        char[] chars = new char[bytes.length / Character.SIZE];
        System.arraycopy(bytes, 0, chars, 0, bytes.length);
        return new String(chars);
    }

    // TODO: fix this
    public static byte[] convertintToBytes( final int i ) {
        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.putInt(i);
        return bb.array();
    }

}
